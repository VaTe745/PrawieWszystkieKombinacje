#include <iostream>
#include <algorithm>
#include <fstream>

using namespace std;

int main() {
	setlocale(LC_ALL, "");
	cout << "Podaj słowo, litery, liczby czy cokolwiek chcesz (nie stosując spacji): \n \n";
	string characters;
	cin >> characters;
	cout << "\n" << "Kombinacje są teraz wypisywane do pliku... \n";
	ofstream combinations("kombinacje.txt");
	sort(characters.begin(), characters.end());
	do
		combinations << characters << "\n";
	while (next_permutation(characters.begin(), characters.end()));
	cout << "Gotowe." << "\n \n";
	system("PAUSE");
	return 0;
}
